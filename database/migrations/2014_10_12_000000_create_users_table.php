<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string            ('name');
            $table->string            ('country_code', 5)->default('020');
            $table->string            ('phone', 15)->unique()->index();
            $table->string            ('code', 10)->nullable();
            $table->timestamp         ('code_expire')->nullable();
            $table->string            ('lang', 2)->default('ar');
            $table->string            ('image', 50)->default('default.png');
            $table->boolean           ('active')->default(0);
            $table->boolean           ('is_blocked')->default(0);
            $table->string            ('email')->unique();
            $table->timestamp         ('email_verified_at')->nullable();
            $table->string            ('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
