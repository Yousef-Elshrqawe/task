<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Http\Resources\Api\Users\UserResource;
use App\Traits\fixPhoneTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable ,UploadTrait , fixPhoneTrait , \App\Traits\TwilioTrait;

    protected $fillable = [
        'name',
        'country_code',
        'phone',
        'code',
        'code_expire',
        'lang',
        'image',
        'active',
        'is_blocked',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    // set phone number format
    public function setPhoneAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['phone'] = $this->fixPhone($value);
        }
    }



    // set country code format
    public function setCountryCodeAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['country_code'] = $this->fixPhone($value);
        }
    }

    public function getFullPhoneAttribute()
    {
        if (array_key_exists("country_code", $this->attributes)){
            $FullPhone = $this->attributes['country_code'] . $this->attributes['phone'];
        }else{
            $FullPhone = 020 . $this->attributes['phone'];
        }

        return $FullPhone;
    }

    // get image from storage
    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $image = $this->getImage($this->attributes['image'], 'users');
        } else {
            $image = $this->defaultImage('users');
        }
        return $image;
    }

    // set image in storage
    public function setImageAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['image']) ? $this->deleteFile($this->attributes['image'], 'users') : '';
            $this->attributes['image'] = $this->uploadAllTyps($value, 'users');
        }
    }

    // set password in storage
    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }


    // send verification code
    public function sendVerificationCode()
    {
        $this->update([
            'code'        => mt_rand(1111, 9999),
            'code_expire' => Carbon::now()->addMinutes(10),
        ]);

        $this->sendSMS($this->full_phone, $this->code);

    }

    // check if code is expired
    public function markAsActive()
    {
        $this->update(['code' => null, 'code_expire' => null, 'active' => true]);
        return $this;
    }


    // login user
    public function login()
    {
        $token = Auth::guard('api')->login($this);
        return UserResource::make($this)->setToken($token);
    }


    // logout user
    public function logout()
    {
        Auth::guard('api')->logout();
    }



    // get user token
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function boot()
    {
        parent::boot();
        /* creating, created, updating, updated, deleting, deleted, forceDeleted, restored */

        static::deleted(function ($model) {
            deleteFile($model->attributes['image'], 'users');
        });
    }
}
