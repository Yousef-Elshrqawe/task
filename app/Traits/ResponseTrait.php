<?php

namespace App\Traits;

use App\Http\Resources\Api\Users\UserResource;

trait ResponseTrait {

  /**
   * keys : success, fail, needActive, waitingApprove, unauthenticated, blocked, exception
   */
  //todo: user builder design pattern
  public function response($key, $msg, $data = [], $anotherKey = [], $page = false) {

    $allResponse['key'] = (string) $key;
    $allResponse['msg'] = (string) $msg;

    # unread notifications count if request ask
    if ('success' == $key && request()->has('count_notifications')) {
      $count = 0;
      if (auth()->check()) {
        $count = auth()->user()->notifications()->unread()->count();
      }

      $allResponse['count_notifications'] = $count;
    }

    # additional data
    if (!empty($anotherKey)) {
      foreach ($anotherKey as $otherkey => $value) {
        $allResponse[$otherkey] = $value;
      }
    }

    # res data
     // [] != $data // commented
    if ( (in_array($key, ['success', 'needActive', 'exception']))) {
      $allResponse['data'] = $data;
    }

    return response()->json($allResponse);
  }

  public function unauthenticatedReturn() {
    return $this->response('unauthenticated', 'يرجى اعادة تسجيل الدخول');
  }

  public function unauthorizedReturn($otherData) {
    return $this->response('unauthorized', 'لا تمتلك هذه الصلاحية', [], $otherData);
  }

  public function blockedReturn($user) {
    $user->logout();
    return $this->response('blocked', 'تم حظر حسابك من قبل الاداره');
  }

  public function phoneActivationReturn($user) {
    $user->sendVerificationCode();
    $userData = new UserResource($user->refresh());
    return $this->response('needActive', ' هذا الحساب غير مفعل تم ارسال كود التفعيل', ['user' => $userData]);
  }

  public function failMsg($msg) {
    return $this->response('fail', $msg);
  }

  public function successMsg($msg = 'done') {
    return $this->response('success', $msg);
  }

  public function successData($data) {
    return $this->response('success', 'تم الارسال بنجاح', $data);
  }

  public function successOtherData(array $dataArr) {
    return $this->response('success', 'تم الارسال بنجاح', [], $dataArr);
  }

  public function getCode($key) {
    switch ($key) {
    case 'success':
      $code = 200;
      break;
    case 'fail':
      $code = 400;
      break;
    case 'needActive':
      $code = 203;
      break;
    case 'unauthorized':
      $code = 400;
      break;
    case 'unauthenticated':
      $code = 401;
      break;
    case 'blocked':
      $code = 423;
      break;
    case 'exception':
      $code = 500;
      break;

    default:
      $code = 200;
      break;

    }

    return $code;
  }


    public function scopeActiveImage($query) {
        return $query->where('active', 1)->where('image', '!=', null);
    }


}
