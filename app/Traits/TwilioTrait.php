<?php

namespace App\Traits;


trait TwilioTrait
{

        public function sendSms($phone, $msg)
        {

            $sid    = env('TWILIO_SID');
            $token  = env('TWILIO_TOKEN');
            $twilio = new \Twilio\Rest\Client($sid, $token);

            $message = $twilio->messages
                ->create("+" . $phone, // to
                    array(
                        "from" => "+15075025180",
                        "body" => $msg
                    )
                );


        }
}
