<?php

namespace App\Traits;


trait fixPhoneTrait
{

    public function fixPhone($string = null){
        if (!$string) {
            return null;
        }
        $result = $this->convert2english($string);
        $result = ltrim($result, '00');
        $result = ltrim($result, '0');
        $result = ltrim($result, '+');
        return $result;
    }

    public function convert2english($string)
    {
        $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $english = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic, $english, $string);
    }
}
