<?php

namespace App\Http\Middleware\Api\Admin;

use App;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminJwtMiddleware {
use \App\Traits\ResponseTrait;
    public function handle($request, Closure $next) {
        if (!Auth::guard('admin')->check()) {

            return $this->response('fail','يرجى تسجيل الدخول');
        }

        return $next($request);
    }
}
