<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Http\Request;

class UpdateProfileRequest extends BaseApiRequest
{
    public function __construct(Request $request)
    {
        $request['phone']        = fixPhone($request['phone']);
        $request['country_code'] = fixPhone($request['country_code']);
        if ($request['phone'] && auth()->user()->phone !== $request['phone']) {
            $request['active'] = false;
        }
    }

    public function rules()
    {
        return [
            'image'         => 'nullable',
            'name'          => 'sometimes|required|max:50',
            'country_code'  => 'sometimes|required|numeric|digits_between:2,5',
            'phone'         => 'sometimes|required|numeric|digits_between:9,10|unique:users,phone,' . auth()->id(),
            'active'        => '',
            'id_number'     => 'sometimes|required|numeric|digits:14|unique:users,id_number,' . auth()->id(),
        ];
    }

    public function withValidator($validator)
    {

    }

}
