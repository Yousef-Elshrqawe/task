<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\BaseApiRequest;
use App\Traits\fixPhoneTrait;
use Illuminate\Http\Request;

class SignUpUserRequest extends BaseApiRequest {
    use fixPhoneTrait;

    public function __construct(Request $request) {
        $request['phone']          = $this->fixPhone($request['phone']);
        $request['country_code']   = $this->fixPhone($request['country_code']);
    }

    public function rules() {
        return [
            'name'         => 'required|max:50',
            'country_code' => 'nullable|numeric|digits_between:2,5',
            'phone'        => 'required|numeric|digits_between:9,10|unique:users,phone',
            'email'        => 'required|email|unique:users,email|max:50',
            'password'     => 'required|min:6|max:100',
            'image'        => 'nullable',
            'lang'         => 'nullable|in:ar,en',
        ];
    }

}
