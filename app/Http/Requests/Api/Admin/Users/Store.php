<?php

namespace App\Http\Requests\Api\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Store extends FormRequest
{
    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
        $request['country_code'] = fixPhone($request['country_code']);
    }

    public function rules() {
        return [
            'name'         => 'required|max:50',
            'country_code' => 'nullable|numeric|digits_between:2,5',
            'phone'        => 'required|numeric|digits_between:9,10|unique:users,phone',
            'email'        => 'required|email|unique:users,email|max:50',
            'password'     => 'required|min:6|max:100',
            'image'        => 'nullable',
            'lang'         => 'nullable|in:ar,en',
            'active'       => 'nullable|in:0,1',
            'is_blocked'   => 'nullable|in:0,1',
        ];
    }
}
