<?php

namespace App\Http\Controllers\Api\Dashboard;



use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admin\Admin\Store;
use App\Http\Requests\Api\Admin\Admin\Update;
use App\Http\Resources\Api\Admin\Admin\AdminResource;
use App\Models\Admin;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    use ResponseTrait, PaginationTrait;

    public function index()
    {
        $admins     = Admin::latest()->paginate(30);
        $adminsData = AdminResource::collection($admins);
        $pagination = $this->paginationModel($admins);
        return      $this->successOtherData(['Store' => $admins]);
    }

    public function store(Store $request)
    {
        $admin   = Admin::create($request->validated());
        $data    = new AdminResource($admin);
        return   $this->successOtherData(['Dashboard' => $data]);
    }

    public function update($id, Update $request)
    {
        $admin   = Admin::findOrFail($id);
        $admin   ->update($request->validated());
        $data    = new AdminResource($admin);
        return   $this->successOtherData(['Dashboard' => $data]);
    }

    public function show($id)
    {
        $admin  = Admin::findOrFail($id);
        $data   = new AdminResource($admin);
        return  $this->successOtherData(['Dashboard' => $data]);
    }
    public function destroy($id)
    {
        if (1 == $id) return $this->successMsg('لا يمكن حذف هذا المدير');
        Admin ::findOrFail($id)->delete();
        return $this->successMsg('تم حذف المدير بنجاح ');
    }
    public function destroyAll(Request $request)
    {
        $requestIds = array_column($request->data, 'id');
        Admin ::whereIn('id', $requestIds)->where('id', '!=', 1)->get()->each->delete();
        return $this->successMsg('تم حذف المديرين بنجاح ');
    }
    public function search(Request $request)
    {
        $admins = Admin::where('name', 'like', '%' . $request->search . '%')
            ->orWhere('email', 'like', '%' . $request->search . '%')
            ->orWhere('phone', 'like', '%' . $request->search . '%')
            ->paginate(10);
        $adminsData = AdminResource::collection($admins);
        $pagination = $this->paginationModel($admins);
        return $this->successOtherData(['admins' => $adminsData, 'pagination' => $pagination]);
    }

}
