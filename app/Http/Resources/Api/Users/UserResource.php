<?php

namespace App\Http\Resources\Api\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    private $token               = '';

    public function setToken($value) {
        $this->token = $value;
        return $this;
    }

    public function toArray($request)
    {
        return [
            'id'                              => $this->id,
            'name'                            => $this->name,
            'phone'                           => $this->phone,
            'country_code'                    => $this->country_code,
            'email'                           => $this->email,
            'lang'                            => $this->lang,
            'active'                          => $this->active ? 'مفعل' : 'غير مفعل',
            'is_blocked'                      => $this->is_blocked ? 'محظور' : 'غير محظور',
            'image'                           => $this->image,
            'token'                           => $this->when($this->token,$this->token),
        ];
    }
}
