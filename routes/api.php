<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Dashboard\AdminController;
use App\Http\Controllers\Api\Dashboard\AdminUserController;
use App\Http\Controllers\Api\Dashboard\AuthAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['namespace' => 'Api', 'middleware' => ['api-lang']], function () {

    /***************************** AuthController Start *****************************/

    Route::post('register',             [AuthController::class, 'Register']);
    Route::post('activate',             [AuthController::class, 'activate']);
    Route::post('resend-code',          [AuthController::class, 'resendCode']);
    Route::post('login',                [AuthController::class, 'login']);
    Route::delete('sign-out',           [AuthController::class, 'logout']);
    Route::post('check-phone',          [AuthController::class, 'checkPhone']);
    Route::post('reset-password',       [AuthController::class, 'resetPassword']);

    Route::group(['middleware' => ['auth:api', 'is-active']], function () {
        Route::get('profile',                    [AuthController::class, 'getProfile']);
        Route::post('update-profile',            [AuthController::class, 'updateProfile']);
        Route::post('update-password',           [AuthController::class, 'updatePassword']);
    });
    /***************************** AuthController end *****************************/
});





/***************************** Dashboard Start *****************************/
Route::group(['prefix' => 'admin', 'namespace'=> 'Dashboard', 'as' => 'admin.'], function () {

    /***************************** Auth Dashboard Start *****************************/
    Route::post('login-admin',          [AuthAdminController::class, 'login']);
    Route::post('logout',               [AuthAdminController::class, 'logout'])->middleware('auth:admin');
    Route::get ('profile-admin',        [AuthAdminController::class, 'profile'])->middleware('auth:admin');

    /***************************** Auth Dashboard End *****************************/

    Route::group(['middleware' => ['Dashboard']], function () {

        /***************************** Dashboard Start *****************************/
        Route::get('admins',                     [AdminController::class, 'index']);
        Route::post('admins/store',              [AdminController::class, 'store']);
        Route::post('admins/update/{id}',        [AdminController::class, 'update']);
        Route::get('admins/{id}',                [AdminController::class, 'show']);
        Route::post('admins/delete/{id}',        [AdminController::class, 'destroy']);
        Route::post('admins/delete-all',         [AdminController::class, 'destroyAll']);
        Route::get('search/admins',              [AdminController::class, 'search']);
        /***************************** Dashboard End *****************************/

        /***************************** User Start *****************************/
        Route::get('users',                      [AdminUserController::class, 'index']);
        Route::get('users/{id}',                 [AdminUserController::class, 'show']);
        Route::post('users/store',               [AdminUserController::class, 'store']);
        Route::post('users/update/{id}',         [AdminUserController::class, 'update']);
        Route::post('users/delete/{id}',         [AdminUserController::class, 'destroy']);
        Route::post('users/delete-all',          [AdminUserController::class, 'destroyAll']);
        Route::post('search/users',              [AdminUserController::class, 'search']);
        /***************************** User end *****************************/
    });


});







/***************************** Dashboard End *****************************/
